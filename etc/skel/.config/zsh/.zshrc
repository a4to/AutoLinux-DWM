#########################################################################################################################################################################################
#########################################################################################################################################################################################
###                                                                                _______| |__                                                                                       ###
##                                                                                |_  / __| '_ \                                                                                       ##
###                                                                                / /\__ \ | | |                                                                                     ###
##                                                                                /___|___/_| |_|                                                                                      ##
###                                                                                                                                                                                   ###
#########################################################################################################################################################################################
##########################################################################   --->  Z-SHELL CONFIG  <---   ###############################################################################
#########################################################################################################################################################################################

                                                                   ## ###  DO NOT REMOVE THE FOLLOWING LINES  ### ##

                                                                   source '/usr/share/lfp/lfpcd' >/dev/null 2>&1
                                                                 source "$HOME"/.config/zsh/zcore.zsh >/dev/null 2>&1
                                                                 source "$HOME"/.config/shell/functionrc >/dev/null 2>&1
                                                                 source "$HOME"/.config/shell/markdir >/dev/null 2>&1
                                                                 source "$HOME"/.config/shell/markfile >/dev/null 2>&1
                                                                 source "$HOME"/.config/shell/tmpdir >/dev/null 2>&1
                                                                 source "$HOME"/.zprofile >/dev/null 2>&1 >/dev/null 2>&1
                                                                  source /usr/share/zsh/plugins/betterzsh.zsh >/dev/null 2>&1
                                                                    source /con/nvx1/.config/nav/config >/dev/null 2>&1

 ### # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ### ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ###
 ### # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ### ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ###


                                                                     ## ###  UNCOMMENT TO ENABLE FEATURES  ### ##



                                                                       #---------------------------------------#
                                                                      # #  --> SHELL PROMPTS AND HEADERS <--  # #
                                                                       #---------------------------------------#


#                                                              Below are listed the available shell prompts and headers,
#                                                            It is recommended to run the alias commands referenced below
#                                                           in opposed to changing or uncommenting the following lines below.

#                                                            This will end up keeping your zshrc much neater, as well as
#                                                                             assist in avoiding errors.




                       #-------------------------#
                      # # --> SHELL HEADERS <-- # #
                       #-------------------------#

##                            Active Header :

                                dateHeader





##     Available shell prompts :

# barHeader      #  ( alias --> bar )
# colHeader      #  ( alias --> bar )
# uniHeader      #  ( alias --> bar )



                       #-------------------------#
                      # # --> SHELL PROMPTS <-- # #
                       #-------------------------#

##                            Active Prompt :

                        eval $(starship init zsh)




##     Available shell prompts :

# prompt xxx      ( alias --> xxx  )
# prompt soul     ( alias --> soul )
# prompt nu1l     ( alias --> nu1l )
# prompt luke     ( alias --> luke )
# prompt void     ( alias --> void )

# star            ( alias --> star )

##   NOTE: The starship prompt must be
##         installed in prder for the star or
##         (starship) prompt to take effect.



 ### # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ### ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ###

                                                                              ## ##    SCRIPTS    ## ##

 ### # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ### ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ###

## Change directories automatically when exiting lf,
## as well as bind 'Ctrl + o' to run lfcd.

blue(){ echo -e "$(tput bold; tput setaf 6)${*}$(tput sgr0)"; }
green(){ echo -e "$(tput bold; tput setaf 2)${*}$(tput sgr0)"; }
yellow(){ echo -e "$(tput bold; tput setaf 3)${*}$(tput sgr0)"; }
red(){ echo -e "$(tput bold; tput setaf 1)${*}$(tput sgr0)"; }
white(){ echo -e "$(tput bold; tput setaf 7)${*}$(tput sgr0)"; }
pink(){ echo -e "$(tput bold; tput setaf 5)${*}$(tput sgr0)"; }
bluen(){ echo -en "$(tput bold; tput setaf 6)${*}$(tput sgr0)"; }
greenn(){ echo -en "$(tput bold; tput setaf 2)${*}$(tput sgr0)"; }
yellown(){ echo -en "$(tput bold; tput setaf 3)${*}$(tput sgr0)"; }
redn(){ echo -en "$(tput bold; tput setaf 1)${*}$(tput sgr0)"; }
whiten(){ echo -en "$(tput bold; tput setaf 7)${*}$(tput sgr0)"; }
pinkn(){ echo -en "$(tput bold; tput setaf 5)${*}$(tput sgr0)"; }


# /usr/bin/wal -f $HOME/.local/share/Nu1LL1nuX/colorSchemes/74.col>/dev/null 2>&1

## Main Key Bindings :

bindkey -s '^o' 'lfp\n'
bindkey -s '^x' 'lfp\n'
bindkey -s '^p' 'mkmci\n'
bindkey -s '^v' 'pulsemixer\n'
bindkey -s "^z" 'clear&&dateHeader\n'
bindkey -s "^L" 'clear\n'
bindkey -s "^k" 'zrc\n'
bindkey -s "^n" 'vV\n'
bindkey -s "^s" 'ccrc\n'
bindkey -s "^f" 'fzfp\n'
bindkey -s "^a" 'arc\n'
bindkey -s "^\\" "v\ $TMPD/$$\n"
bindkey -s '<a-w>' 'qt\n'
bindkey -s '^r' 'gacp\n'
bindkey -s '^u' 'wp34\n'
bindkey -s '^Y' 'conmake\n'
bindkey -s '^x' 'nvim\ $HOME/.config/shell/functionrc\n'
bindkey -s '^u' 'mkbuild\n'
bindkey -s '^g' 'pushto\n'
bindkey -s "^_" 'fav\n'
bindkey -s "^^" 'reserv\n'
bindkey -s "^h" 'navmenu\n'
bindkey -s "^]" 'mkd2\n'
#bindkey -s "^b" '#RESERVED\n'

export VOLTA_HOME="$HOME/.volta"
export PATH="$VOLTA_HOME/bin:$PATH"

 ### # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ### ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ###
 ### # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ### ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ### ## ### # ### ## ## # ###
[ -f /home/nvx1/.config/shell/gitliasrc ] && source /home/nvx1/.config/shell/gitliasrc
[ -f /home/nvx1/.config/shell/gitliasrc ] && source /home/nvx1/.config/shell/gitliasrc

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/nvx1/DATADIR/16.08.2022/11.09/google-cloud-sdk/path.zsh.inc' ]; then . '/home/nvx1/DATADIR/16.08.2022/11.09/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/home/nvx1/DATADIR/16.08.2022/11.09/google-cloud-sdk/completion.zsh.inc' ]; then . '/home/nvx1/DATADIR/16.08.2022/11.09/google-cloud-sdk/completion.zsh.inc'; fi



export GPGKEY='9A1F3F41DDA55A60331CB8DD3714CB0967D15980'






export GPG_TTY=$(tty)
