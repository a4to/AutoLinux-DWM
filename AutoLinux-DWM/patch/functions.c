
#define SH(cmd) (const char*[]){ "/bin/sh", "-c", cmd, NULL }
#define ROFI() (const char*[]){ "/bin/rofi", "-show", "run", NULL }
#define TERM_RUN(cmd) (const char*[]){ "/bin/alacritty", "-e", cmd, NULL }


void runcmd(const char *cmd[]) {
  if (fork() == 0) {
    setsid();
    execvp(cmd[0], (char**)cmd);
  }
}

void runsh(const char *cmd) {
  runcmd(SH(cmd));
}

void termrun(const char *cmd) {
  runcmd(TERM_RUN(cmd));
}

