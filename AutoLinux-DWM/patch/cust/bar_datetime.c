int
width_datetime(Bar *bar, BarWidthArg *a)
{
  return TEXTW("DDD, DD MMM   00:00  ") + lrpad;
}

int
draw_datetime(Bar *bar, BarDrawArg *a)
{
	int boxs = drw->fonts->h / 9;
	int boxw = drw->fonts->h / 6 + 2;
	int x = a->x, w = a->w;
	Monitor *m = bar->mon;

	drw_setscheme(drw, scheme[m == selmon ? SchemeActive : SchemeInactive]);

  char date[100];
  get_date(date);
  date[strlen(date) - 1] = '\0';

  char time[100];
  get_time(time);
  time[strlen(time) - 1] = '\0';

  char date_time[100];
  strcpy(date_time, date);
  strcat(date_time, time);

  drw_text(drw, a->x, 0, a->w, bh, lrpad / 2, date_time, 0);
  drw_map(drw, bar->win, 0, 0, w, bh);

  return x + w;
}

