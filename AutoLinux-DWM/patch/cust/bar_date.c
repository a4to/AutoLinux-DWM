
// get current time once a second
void get_date(char *time) {
    FILE *fp;
    char buf[100];
    fp = popen("echo -en \"  `date '+%a, %d %b'`\"", "r");
    fgets(buf, 100, fp);
    pclose(fp);
    strcpy(time, buf);
}

int
width_date(Bar *bar, BarWidthArg *a)
{
  return TEXTW("DDD, DD MMM   ") + lrpad;
}

int
draw_date(Bar *bar, BarDrawArg *a)
{
	int boxs = drw->fonts->h / 9;
	int boxw = drw->fonts->h / 6 + 2;
	int x = a->x, w = a->w;
	Monitor *m = bar->mon;

	drw_setscheme(drw, scheme[m == selmon ? SchemeInactive : SchemeNorm]);

  char date[100];
  get_date(date);
//  date[strlen(date) - 1] = '\0';

  drw_text(drw, a->x, 0, a->w, bh, lrpad / 2, date, 0);
  drw_map(drw, bar->win, 0, 0, w, bh);

  return x + w;
}

int
click_date(Bar *bar, Arg *arg, BarClickArg *a)
{
  runsh("rfi 40");
  return EXIT_SUCCESS;
}
